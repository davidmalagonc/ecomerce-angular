import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Producto } from './models/producto';

@Injectable({
  providedIn: 'root'
})
export class ComunicationService {
  mensaje: Producto = new Producto('', 0, 0, 0);
  private enviarMensajeSubject = new Subject<Producto>();
  enviarMensajeObservable = this.enviarMensajeSubject.asObservable();

  enviarMensaje(mensaje: Producto) {
    this.mensaje = mensaje;
    this.enviarMensajeSubject.next(mensaje);
  }
}
