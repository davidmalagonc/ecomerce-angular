import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import axios from 'axios';
import { ResponseLogin } from '../models/responseLogin';
import { serverURL } from '../../../config/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string = '';
  nombre: string = '';
  password: string = '';
  urlServer = serverURL;
  constructor(private routes: Router) {

  }

  ngOnInit(): void {
  }

  async login() {

    await axios.post(this.urlServer + `/login?email=${this.email}&password=${this.password}`).then(response => {
      // Respuesta del servidor
      console.log(response)
      if (response.data !== '') {
        alert('LOGIN CORRECTO')
        localStorage.setItem('USER', this.email);
        this.routes.navigate(['/']);
      }else{
        alert('LOGIN INCORRECTO')
      }

    }).catch(e => {
      console.log(e);
    });
    

  }
  register() {
    this.routes.navigate(['/register']);
  }

}
