export class Producto {
    nombre: string;
    valor: number;
    iva: number;
    cantidad: number;

    constructor(nombre: string, valor: number, iva: number, cantidad: number) {
        this.nombre = nombre;
        this.valor = valor;
        this.iva = iva;
        this.cantidad = cantidad;
    }
}