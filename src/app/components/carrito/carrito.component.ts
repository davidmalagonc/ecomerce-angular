import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {
  valorLentejas = 3000;
  ivaLentejas = 0;
  valorGaseosa = 2900;
  ivaGaseosa = 1000;
  valorPasta = 4000;
  ivaPasta = 700;
  valorArroz = 1400;
  ivaArroz = 500;
  valorCafe = 4000;
  ivaCafe = 1000;

  constructor() { }

  ngOnInit(): void {
  }

}
