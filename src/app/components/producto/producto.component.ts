import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ComunicationService } from '../comunication.service';
import { Producto } from '../models/producto';


@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  @Input() ima: string = '';
  @Input() nombre: string = '';
  @Input() valor: string = '';
  @Input() iva: string = '';




  constructor(private servicioComunicacion: ComunicationService) {

  }

  sendToList() {
    this.servicioComunicacion.enviarMensaje(new Producto(this.nombre, Number(this.valor), Number(this.iva), 1))
    //console.log(this.nombre + '\t' + this.valor + '\t' + this.iva)
  }

  ngOnInit() {

  }

}
