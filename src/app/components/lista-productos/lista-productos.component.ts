import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ComunicationService } from '../../components/comunication.service';
import { Producto } from '../models/producto';
import { serverURL } from '../../../config/constants';
import * as CanvasJS from 'canvasjs';
import axios from 'axios';

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.css']
})
export class ListaProductosComponent implements OnInit {

  productosgraf = [
    { y: 1, label: 'dat' }
  ]



  constructor(private comunicationService: ComunicationService, private router: Router) { }
  productos: Producto[] = [];
  total: number = 0;
  iva: number = 0;
  subtotal: number = 0;


  ngOnInit() {
    this.comunicationService.enviarMensajeObservable.subscribe(producto => {
      this.addDataToList(producto);
    });


    var productPre = localStorage.getItem('lista');
    if(productPre !== null){
      this.productos = JSON.parse(productPre)
      this.productos.forEach(producto =>{
        this.subtotal += producto.valor * producto.cantidad;
        this.iva += producto.iva;
      })
    }


  }

  addDataToList(producto: Producto) {

    let existe = false;

    this.productos.forEach(product => {
      if (product.nombre === producto.nombre) {
        existe = true;
      }
    });

    if (!existe) {
      this.productos.push(producto);
      this.subtotal += producto.cantidad * producto.valor
      this.total += this.subtotal + producto.iva
      this.iva += producto.iva;
    }

  }

  cambiarCantidad(nombre: string, cantidad: number) {

    let index = 0;
    this.productos.forEach(product => {
      if (product.nombre === nombre) {
        if (this.productos[index].cantidad + cantidad <= 0) {
          this.subtotal -= this.productos[index].valor;
          this.iva -= product.iva;
          this.productos.splice(index, 1);

        } else {
          product.cantidad += cantidad;
          this.iva += product.iva;
          this.subtotal += cantidad * this.productos[index].valor
        }
      }
      index++;
    });
    
    this.printGraft();

  }

  printGraft() {
    this.productosgraf = [];
    this.productos.forEach(producto => {
      this.productosgraf.push({ y: producto.cantidad, label: producto.nombre })
    })

    let chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: 'Cantidad de compras realizadas'
      },
      data: [{
        type: "column",
        dataPoints: this.productosgraf
      }]
    });
    chart.render();
  }
  async buyList() {
    if (localStorage.getItem('USER') != null) {

      if (this.productos.length > 0) {
        await axios.post(serverURL + `/savePurchase`,
          {
            idUser: localStorage.getItem('USER'),
            product: this.productos,
            subTotal: this.subtotal,
            total: this.subtotal + this.iva
          }).then(response => {
            console.log(response)
          }).catch(e => {
            console.log(e);
          });
      }


      alert('SE va a enviar tu lista de productos a tu cuenta')
    } else {
      if (this.productos.length > 0) {
        console.log(this.productos)
        localStorage.setItem('lista', JSON.stringify(this.productos))
        alert('Debes iniciar sesión primero')
        this.router.navigate(['/login']);
      }
      else {
        alert('COMPRA ALGO ANTES DE CONTINUAR')
      }
    }

  }

}
