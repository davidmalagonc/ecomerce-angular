import { Component, OnInit } from '@angular/core';
import { serverURL } from '../../../config/constants';
import axios from 'axios';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email: string = '';
  nombre: string = '';
  password: string = '';

  constructor(private routes: Router) { }

  ngOnInit(): void {
  }

  async register() {
    await axios.post(serverURL + '/register', {
      email: this.email,
      password: this.password,
      name: this.nombre
    }).then(response => {
      if(response){
        console.log(response);
        localStorage.setItem('USER', this.email)
        this.routes.navigate(['/']);
      }
    }).catch(e =>{
      console.log(e);
    })
  }

}
