import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'carritoExample';
  loged: boolean;


  signOut() {
    localStorage.removeItem('USER');
    localStorage.clear();
    location.reload();

  }

  constructor() {
    this.loged = false;
  }
  ngOnInit() {
    if (localStorage.getItem('USER') != null) {
      this.loged = true;
    } else {
      this.loged = false;
    }
  }


}
